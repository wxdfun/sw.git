# sw

#### 介绍
Spring Cloud Gateway整合Swagger2 Demo，全网首例，其他项目基本都是照搬我代码的。包括knife4j。当然也欢迎大家为开源贡献自己的一份力量

#### 软件架构
Spring Cloud


#### 安装教程

1. 运行eureka
2. 运行admin
3. 运行gateway

#### 使用说明

浏览器访问：http://localhost:3333/swagger-ui.html

博客地址：https://blog.csdn.net/ttzommed/article/details/81103609